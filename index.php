<?php
  /**
   * The main template file.
   *
   * This is the most generic template file in a WordPress theme
   * and one of the two required files for a theme (the other being style.css).
   * It is used to display a page when nothing more specific matches a query.
   * E.g., it puts together the home page when no home.php file exists.
   *
   * @link https://codex.wordpress.org/Template_Hierarchy
   *
   * @package waterview
   */
  
  get_header(); ?>
<section class="blogpost">
  <div class="container">
    <div class="row">
      <div class="col-md-9">
        <div class="blog-article">
          <?php if( have_posts() ) : ?>
          <?php while( have_posts() ) : the_post(); ?>
          <div class="col-md-6">
            <?php get_template_part('template-parts/content', get_post_format()); ?>
          </div>
          <?php endwhile; ?>
          <?php else : ?>
          <h2>No Post Found</h2>
          <?php endif; ?>
        </div>
        <div class="post-pagination text-center"> 
          <?php the_posts_pagination(array(
            'next_text' => '<span aria-hidden="true">Next <i class="fa fa-angle-right" aria-hidden="true"></i></span>',
            'prev_text' => '<span aria-hidden="true"> <i class="fa fa-angle-left" aria-hidden="true"></i> Prev </span>',
            'screen_reader_text' => ' ',
            'type'                => 'list'
            )); ?>
        </div>
      </div>
      <div class="col-md-3">
        <?php get_sidebar(); ?>
      </div>
    </div>
  </div>
</section>
<?php get_footer();

