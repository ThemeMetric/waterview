<?php 
  /*
  Template Name: Home page
  
  */
  ?>
<?php get_header(); ?>
<section class="mainslider">
  <div class="container">
    <div class="row">
      <div class="col md-12">
        <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
          <!-- Wrapper for slides -->
          <div class="carousel-inner" role="listbox">
            <div class="item active">
              <img src="<?php  echo $waterview_options['slider-img-one'][url]?>" alt="" />
            </div>
            <div class="item">
              <img src="<?php  echo $waterview_options['slider-img-two'][url]?>" alt="" />
            </div>
            <div class="item">
              <img src="<?php  echo $waterview_options['slider-img-three'][url]?>" alt="" />
            </div>
             <div class="item">
              <img src="<?php  echo $waterview_options['slider-img-four'][url]?>" alt="" />
            </div>
             <div class="item">
              <img src="<?php  echo $waterview_options['slider-img-five'][url]?>" alt="" />
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<section class="main_content" id="servicess">
  <div class="container">
    <div class="row">
      <div class="col-md-9">
        
            <?php 
              $serviceitem = new WP_Query(array(
                'post_type' => 'service',
                'orderby'=> 'menu_order',
              
              ));
              
              ?>
            <?php while( $serviceitem->have_posts() ) :  $serviceitem->the_post(); ?>
	
          
            <div class="col-md-6">
              <div class="col-md-6 text-center">
                <div class="service_img"> 
                  <a href="<?php the_permalink(); ?>"><?php the_post_thumbnail(); ?></a>
                </div>
              </div>
              <div class="col-md-6">
                <div class="service_content">
                  <a href="<?php the_permalink(); ?>">
                    <h2><?php the_title() ; ?> </h2>
                  </a>
                  <p><?php echo wp_trim_words( get_the_content(), 10, ' ' ); ?> <a href="<?php the_permalink(); ?> " class="readmore">Read More</a> </p>
                </div>
              </div>
            </div>
			  
       
            <?php endwhile; ?>
			
       
        <?php while (have_posts() ) : the_post() ;  ?>
        <div class="home_content"> 
          <?php the_content() ;  ?>
        </div>
        <?php endwhile;  ?>
      </div>
      <div class="col-md-3">
        <div class="sidebar"> 
          <?php get_sidebar() ; ?>
        </div>
      </div>
    </div>
  </div>
</section>
<?php get_footer();