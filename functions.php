<?php
/**
 * waterview functions and definitions.
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package waterview
 */

if ( ! function_exists( 'waterview_setup' ) ) :
/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 */
function waterview_setup() {
	/*
	 * Make theme available for translation.
	 * Translations can be filed in the /languages/ directory.
	 * If you're building a theme based on waterview, use a find and replace
	 * to change 'waterview' to the name of your theme in all the template files.
	 */
	load_theme_textdomain( 'waterview', get_template_directory() . '/languages' );

	// Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );

	/*
	 * Let WordPress manage the document title.
	 * By adding theme support, we declare that this theme does not use a
	 * hard-coded <title> tag in the document head, and expect WordPress to
	 * provide it for us.
	 */
	add_theme_support( 'title-tag' );

	/*
	 * Enable support for Post Thumbnails on posts and pages.
	 *
	 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
	 */
	add_theme_support( 'post-thumbnails' );

// This theme uses wp_nav_menu() in one location.
	register_nav_menus( array(
		'primary' => esc_html__( 'Main Menu', 'waterview' ),
		'footer-menu' => esc_html__( 'Footer Menu', 'waterview' ),
	) );
	/*
	 * Switch default core markup for search form, comment form, and comments
	 * to output valid HTML5.
	 */
	add_theme_support( 'html5', array(
		'search-form',
		'comment-form',
		'comment-list',
		'gallery',
		'caption',
	) );

	/*
	 * Enable support for Post Formats.
	 * See https://developer.wordpress.org/themes/functionality/post-formats/
	 */
	add_theme_support( 'post-formats', array(
		'aside',
		'image',
		'video',
		'quote',
		'link',
	) );

	// Set up the WordPress core custom background feature.
	add_theme_support( 'custom-background', apply_filters( 'waterview_custom_background_args', array(
		'default-color' => 'ffffff',
		'default-image' => '',
	) ) );
}
endif;
add_action( 'after_setup_theme', 'waterview_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function waterview_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'waterview_content_width', 640 );
}
add_action( 'after_setup_theme', 'waterview_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function waterview_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar', 'waterview' ),
		'id'            => 'sidebar-right',
		'description'   => esc_html__( 'Add widgets here.', 'waterview' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
    
        
      
}
add_action( 'widgets_init', 'waterview_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function waterview_scripts() {
	wp_enqueue_style( 'waterview-style', get_stylesheet_uri() );
    wp_enqueue_style( 'waterview-bootsrap', get_template_directory_uri().'/css/bootstrap.min.css' );
    wp_enqueue_style( 'waterview-awesome', get_template_directory_uri().'/css/font-awesome.min.css' );
    
    wp_enqueue_style( 'waterview-custom', get_template_directory_uri().'/css/custom.css' );
    wp_enqueue_style( 'waterview-responsive', get_template_directory_uri().'/css/responsive.css' );
	
	
	wp_enqueue_script( 'waterview-bootsrapjs', get_template_directory_uri() . '/js/bootstrap.min.js', array('jquery'), '', true );

	wp_enqueue_script( 'waterview-cutomizerjs', get_template_directory_uri() . '/js/customizer.js', array('jquery'), '', true );
	wp_enqueue_script( 'waterview-nicescroll', get_template_directory_uri() . '/js/jquery.nicescroll.min.js', array('jquery'), '', true );
	wp_enqueue_script( 'waterview-stykyjs', get_template_directory_uri() . '/js/jquery.sticky.js', array('jquery'), '', true );
	wp_enqueue_script( 'waterview-script', get_template_directory_uri() . '/js/script.js', array('jquery'), '', true );


   

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'waterview_scripts' );

/**
 * Nav walker 
 
 */

require get_template_directory() . '/inc/waterview-navwalker.php';


/******************************************************************************/
/***************************** Theme Options **********************************/
/******************************************************************************/

if ( !class_exists( 'ReduxFramework' ) && file_exists( dirname( __FILE__ ) . '/settings/redux/ReduxCore/framework.php' ) ) {
    require_once( dirname( __FILE__ ) . '/settings/redux/ReduxCore/framework.php' );
}
if ( !isset( $redux_demo ) && file_exists( dirname( __FILE__ ) . '/settings/waterview.config.php' ) ) {
    require_once( dirname( __FILE__ ) . '/settings/waterview.config.php' );
}

global $waterview_options;




/******************************************************************************/
/*****************************  Custom post  **********************************/
/******************************************************************************/

// Register Custom Post
add_action( 'init', 'waterview_custom_post' );
function waterview_custom_post() {

  register_post_type( 'service',
    array(
      'labels' => array(
        'name' => __( 'service' ),
        'singular_name' => __( 'Portfolio' ),
        'add_new_item' => __( 'Add New Service ' )
      ),
    'public' => true,
	'supports' => array('title', 'editor', 'thumbnail'),
    'has_archive' => true,
    )
  );

  
}

