<?php 
  /*
  Template Name: Service Page
  
  */
  ?>
<?php get_header(); ?>
<section class="main_content">
  <div class="container">
   
            <?php 
              $serviceitem = new WP_Query(array(
                'post_type' => 'service',
                'orderby'=> 'menu_order',
              
              ));
              
              ?>
            <?php while( $serviceitem->have_posts() ) :  $serviceitem->the_post(); ?>
            <div class="row">
      
        <div class="service">
          <div class="service_article">
			   <div class="single_servicepage">
              <div class="col-md-3 col-lg-3 col-sm-12 col-xs-12 text-center">
                <div class="service_img boma"> 
                  <a href="<?php the_permalink(); ?>"><?php the_post_thumbnail(); ?></a>
                </div>
              </div>
              <div class="col-md-9 col-lg-9 col-sm-12 col-xs-12">
                <div class="service_content boma">
                  <a href="<?php the_permalink(); ?>">
                    <h2><?php the_title() ; ?> </h2>
                  </a>
                  <p><?php echo wp_trim_words( get_the_content(), 60, ' ' ); ?> <a href="<?php the_permalink(); ?> " class="readmore">Read More</a> </p>
                </div>
              </div>
			  </div>
              </div>
           </div>				
        </div>
        <?php endwhile; ?>  	
  </div>
</section>
<?php get_footer();