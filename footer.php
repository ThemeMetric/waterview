<?php
  /**
   * The template for displaying the footer.
   *
   * Contains the closing of the #content div and all content after.
   *
   * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
   *
   * @package waterview
   */
  
  ?>
     <!-- <div class="scrolltoup text-center">
      <div class="scrollup"><i class="fa fa-angle-double-up"></i></div>
    </div> -->
    <!-- End scroll to up -->
    
    
    	
<div class="scroll-btn-container">
			
     <div style="display:none;" class="scroll-to-top" id="scroll-to-top">
        <img alt="" width="32" height="32" src="<?php echo get_template_directory_uri() ; ?>/img/12_u.ico" > 
     </div>

    <div style="display:block;" class="scroll-to-bottom" id="scroll-to-bottom">
        <img alt="" width="32" height="32" src="<?php echo get_template_directory_uri() ; ?>/img/12_d.ico" >
    </div>
				
</div>	
    
    
    
    
    
<footer class="mainfooter text-center">
  <div class="container">
    <div class="row">
      <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
        <div class="footer_menu">
          <?php 
            wp_nav_menu(array(
              'theme_location'  => 'footer-menu',
              'menu_class'      => 'breadcrumb',
              'walker'          => new waterview_nav_walker()
            
            ));
            ?>
       	  
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
        <div class="copyright_text">
          <?php  global $waterview_options?> 
          <p>  <?php  echo $waterview_options['footer-copyright'] ?> </p>
          <a href="#" onclick="window.print();return false;">Print</a>
        </div>
      </div>
    </div>
  </div>
</footer>
<?php wp_footer(); ?>
</body>
</html>
