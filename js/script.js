
(function($){
    
   "use strict" ;
    
    $(document).ready(function(){
                
    // Sticky Menu
    $(".stickynav").sticky({
        topSpacing: 0
    });
       // carosel 
$('.carousel').carousel({
  interval: 2000
});
    // click to scroll up and bottom
   
    	$(window).scroll(function() {
            if ($(this).scrollTop() > 200) {
               $('#scroll-to-top').fadeIn('slow');
            } else {
                $('#scroll-to-top').fadeOut('slow');
            }

              if ($(this).scrollTop() > $(document).height() - screen.height) {
               $('#scroll-to-bottom').fadeOut('slow');
            } else {
               $('#scroll-to-bottom').fadeIn('slow');
            }

				 });

                 
     $('.scroll-to-top').click(function(){
       $("html,body").animate({
                        scrollTop:0
                        },600);
                  return false;
      
	});
     $('.scroll-to-bottom').click(function(){
      $('html,body').animate({
          scrollTop: $(document).height()},
              600); 
	return false;
	});
                       
        // jQuery Custom scrollbar
		$("body").niceScroll({
			cursorcolor: "rgba(47, 53, 59, 0.95)",
			cursorborderradius: "0px",
			cursorwidth: "10px",
			cursorminheight: 100,
			cursorborder: "0px solid #fff",
			zindex: 9999,
			autohidemode: true,
			horizrailenabled:false
		});
          
         
    });
    
    
})(jQuery);
