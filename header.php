<?php
  /**
   * The header for our theme.
   *
   * This is the template that displays all of the <head> section and everything up until <div id="content">
   *
   * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
   *
   * @package waterview
   */
  global $waterview_options;
  
  ?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
  <head>
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href='https://fonts.googleapis.com/css?family=Roboto:400,300,500,700,100' rel='stylesheet' type='text/css'>
    <link rel="profile" href="http://gmpg.org/xfn/11">
    <link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
    <!-- ******************************************************************** -->
    <!-- * Custom Favicon *************************************************** -->
    <!-- ******************************************************************** -->
    <?php
      if ( (isset($waterview_options['favicon']['url'])) && (trim($waterview_options['favicon']['url']) != "" ) ) {
             
             if (is_ssl()) {
                 $favicon_image_img = str_replace("http://", "https://", $waterview_options['favicon']['url']);		
             } else {
                 $favicon_image_img = $waterview_options['favicon']['url'];
             }
      ?>
    <!-- ******************************************************************** -->
    <!-- * Favicon ********************************************************** -->
    <!-- ******************************************************************** -->
    <link rel="shortcut icon" href="<?php echo esc_url($favicon_image_img); ?>" />
    <?php } ?>
    <?php wp_head(); ?>
  </head>
  <body <?php body_class(); ?>>
    <header class="mainheader" id="mainheader">
      <div class="topbar">
        <div class="container">
          <div class="row">
            <div class="col-md-6">
            </div>
            <div class="col-md-6">
              <div class="topbar_cintent">
                <ul class="navbar-right">
                  <li><i class="fa fa-envelope" aria-hidden="true"></i> <a href="mailto:<?php  echo $waterview_options['waterview-email'] ?>"><?php  echo $waterview_options['waterview-email'] ?></a></li>
                  <li><i class="fa fa-mobile" aria-hidden="true"></i> <?php  echo $waterview_options['waterview-number'] ?></li>
                </ul>
              </div>
            </div>
          </div>
        </div>
      </div>
      <nav class="stickynav">
        <div class="container">
          <!-- Brand and toggle get grouped for better mobile display -->
          <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            </button>
            <div class="logo">
              <a class="navbar-brand" href="<?php bloginfo ('home') ; ?>">
              <img src=" <?php echo $waterview_options['site_logo']['url'] ?>" alt="Waterview" class="img-responsive" />	
              </a>
            </div>
          </div>
         
          <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <?php 
              wp_nav_menu(array(
                'theme_location'  => 'primary',
                'menu_class'      => 'nav navbar-nav navbar-right',
                'walker'          => new waterview_nav_walker()
              
              ));
              ?>
          </div>
        
        </div>
      </nav>
    </header>
