<?php get_header(); ?>
<section class="main_content">
  <div class="container">
    <div class="row">
      <div class="col-md-9 col-lg-9 col-sm-12 col-xs-12">
        <div class="service">
          <div class="service_article">
            <?php while(have_posts() ) : the_post(); ?>
            <div class="service_imgs"> 
                 <a href="<?php the_permalink(); ?>">
                <h2><?php the_title() ; ?> </h2>
              </a>
                <p>
              <a href="<?php the_permalink(); ?>"><?php the_post_thumbnail(); ?></a>
              <?php echo the_content() ; ?>
                </p>
            </div>
            <div class="service_content">
              <a href="<?php the_permalink(); ?>">
                <h2><?php the_title() ; ?> </h2>
              </a>
              <p><?php echo the_content() ; ?></p>
            </div>
            <?php endwhile; ?>
          </div>
        </div>
      </div>
      <div class="col-md-3 col-lg-3 col-sm-12 col-xs-12">
        <div class="sidebar"> 
          <?php get_sidebar() ; ?>
        </div>
      </div>
    </div>
  </div>
</section>
<?php get_footer();
